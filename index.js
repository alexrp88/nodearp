const express = require('express')
const app = express()
// Load xss to enable us to pass user input through xss() to guard against XSS attack
const xss = require('xss')
const http = require('http').Server(app)
const io = require('socket.io')(http)
const sassMiddleware = require('node-sass-middleware')
const path = require('path')
// Max and current users
const maxUsers = 10
let currentUsers = 0
// Variable to hold list of usernames
let users = []

// Add SASS middleware
app.use(sassMiddleware({
    src: path.join(__dirname, '_source_styles'),
    dest: path.join(__dirname, 'public'),
    debug: false,
    outputStyle: 'compressed',
    indentedSyntax: true
}));

app.use(express.static('public'))

io.sockets.on('connection', socket => {
    socket.on('username', username => {
        socket.username = xss(username)
        if (users.includes(socket.username))
        {
            // Emit taken if username is already in users array
            socket.emit('taken')
        }
        else if (currentUsers >= maxUsers) 
        {
            // Emit chat message and hide form if too many people connected
            socket.emit('chat_message','<style>#message-form {display: none}</style>Sorry - there are too many people in the room already! You will be able to view but not send messages.')
        }
        else if (socket.username == '')
        {
            // Emit chat message and hide form if user manages to avoid entering a name
            socket.emit('chat_message','<style>#message-form {display: none}</style>Sorry - if you haven\'t entered a name, you will be able to view but not send messages. <a href="/">Reload the page to try again</a>.')
        }
        else 
        {
            // Tell others in the room that user has joined
            socket.broadcast.emit('chat_message', '<i>' + socket.username + ' joined</i>')
            // Increment current users and add name to users array
            currentUsers++
            users.push(socket.username)
            // Emit users_online to update list displayed in window
            io.emit('users_online',users)
            // Setting loggedin to true will allow the user to send messages
            socket.loggedin = true
        }
    })
 
    socket.on('disconnect', () => {
        if (socket.loggedin)
        { 
            // Decrement current users
            currentUsers--
            // Find user in users array and remove them
            const userPosition = users.indexOf(socket.username)
            users.splice(userPosition, 1)
            // Tell others in the room that they have left
            socket.broadcast.emit('chat_message', '<i>' + socket.username + ' left</i>')
            // Emit users_online to update list displayed in window
            io.emit('users_online',users)
        }
    })

    socket.on('chat_message', message => {
        // Check if user logged in and emit message if they are
        if (socket.loggedin)
        { 
            io.emit('chat_message', '<strong>' + socket.username + '</strong>: ' + xss(message))
        }
    })

    socket.on('is_typing', () => {
        // Check if user logged in and emit is_typing to other users in the room if they are
        if (socket.loggedin)
        { 
            socket.broadcast.emit('is_typing', socket.username)
        }
    })
})

http.listen(3000, () => {
    console.log('Listening on 3000')
})


