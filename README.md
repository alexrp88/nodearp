# README #

This is a simple Node chat app, built as part of the process of learning Node, Express and Socket.io. The template is based on https://itnext.io/build-a-group-chat-app-in-30-lines-using-node-js-15bfe7a2417b.

### Features of this app ###

- ES6 in index.js 
- Using xss module to guard against XSS attacks
- Styled with flex, with message panel on left and user list panel on right
- Uses SASS middleware for easy updates to CSS
- Uses broadcast when new users connect/disconnect or someone is typing
- Maximum number of users in maxUsers variable; current number in currentUsers
- Usernames maintained in users array; duplicates and blank usernames not allowed
- Message form hidden and messaging disabled if user dismisses prompt by preventing page from creating additional dialogs; user invited to refresh page to enter username
